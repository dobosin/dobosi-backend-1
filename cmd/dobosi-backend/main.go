package main

import (
	"log"
	"net/http"

	env "github.com/kelseyhightower/envconfig"

	"gitlab.com/dobosin/dobosi-backend-1/internal/auth"
	"gitlab.com/dobosin/dobosi-backend-1/internal/db"
	"gitlab.com/dobosin/dobosi-backend-1/internal/handler"
)

type Config struct {
	App  *AppConfig
	DB   *db.Config
	Auth *auth.Config
}

type AppConfig struct {
	Port string
}

func main() {
	var cfg Config
	env.Process("", &cfg)

	// Connect to DB
	dbs := db.NewDB(*cfg.DB)
	defer dbs.Pg().Close()
	defer dbs.Redis().Close()

	log.Println("Connected to redis", dbs.RedisAddr())
	log.Println("Connected to postgress", dbs.PgAddr())

	handler.Init(dbs, cfg.Auth)

	log.Println("App has started and listening in the localhost:", cfg.App.Port)
	if err := http.ListenAndServe(":"+cfg.App.Port, nil); err != nil {
		panic(err)
	}
}
