FROM golang:alpine

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

WORKDIR /build

COPY . .
RUN go mod download
RUN go build -o main cmd/dobosi-backend/main.go

WORKDIR /dist
RUN cp /build/main .
RUN cp /build/index.html .
RUN cp /build/registration.html .
RUN cp /build/members.html .

CMD ["/dist/main"]