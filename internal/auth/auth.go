package auth

import (
	"log"
	"time"

	"gitlab.com/dobosin/dobosi-backend-1/internal/db"
	"gitlab.com/dobosin/dobosi-backend-1/internal/response"
)

type Config struct {
	JwtSecret   string `envconfig:"JWT_SECRET"`
	TokenExpire int    `envconfig:"TOKEN_EXPIRE"`
}

type Auth struct {
	DB  db.DB
	cfg *Config
}

func NewAuth(db db.DB, cfg *Config) *Auth {
	a := Auth{
		DB:  db,
		cfg: cfg,
	}
	return &a
}

func (a *Auth) GenerateToken(id string) (string, error) {
	cl := claim{
		id:  id,
		exp: time.Minute * time.Duration(a.cfg.TokenExpire),
	}

	token, err := generateJwtToken(a.cfg.JwtSecret, cl)
	if err != nil {
		log.Println(err)
		return "", response.InternalError
	}

	err = a.DB.Redis().Set(token, id, cl.exp).Err()
	if err != nil {
		log.Println(err)
		err = response.InternalError
	}

	return token, err
}

func (a *Auth) ValidateToken(token string) (string, error) {
	cl, err := parseJwtToken(a.cfg.JwtSecret, token)
	return cl.id, err
}
