package auth

import (
	"log"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type claim struct {
	id  string
	exp time.Duration
}

func generateJwtToken(secret string, cl claim) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	/* Set token claims */
	claims["id"] = cl.id
	claims["exp"] = time.Now().Add(cl.exp).Unix()
	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		log.Fatal("Error in Generating key")
		return "", err
	}
	return tokenString, nil
}

func parseJwtToken(secret string, tokenStr string) (claim, error) {
	var cl claim
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		cl.id = claims["id"].(string)
		return cl, nil
	} else {
		return cl, err
	}
}
