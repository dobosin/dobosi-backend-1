package handler

import (
	"net/http"

	"gitlab.com/dobosin/dobosi-backend-1/internal/db"
	"gitlab.com/dobosin/dobosi-backend-1/internal/response"
	"gitlab.com/dobosin/dobosi-backend-1/internal/user"
)

type UserHandler struct {
	DB db.DB
}

func NewUserHandler(database db.DB) UserHandler {
	return UserHandler{database}
}

func (uh UserHandler) RegisterUser(w http.ResponseWriter, r *http.Request) {
	u, err := user.NewUserFromRequest(uh.DB, r.Body)
	if err != nil {
		response.Send(w, "", err)
		return
	}

	res, err := u.Create()
	response.Send(w, res, err)
}
