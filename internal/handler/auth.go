package handler

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/dobosin/dobosi-backend-1/internal/auth"
	"gitlab.com/dobosin/dobosi-backend-1/internal/db"
	"gitlab.com/dobosin/dobosi-backend-1/internal/response"
	"gitlab.com/dobosin/dobosi-backend-1/internal/user"
)

type AuthHandler struct {
	DB   db.DB
	Auth *auth.Auth
}

type contextData struct {
	id    string
	token string
}

type contextKey string

const authCtx contextKey = "authCtx"
const loggedIn string = "Logged in"
const loggedOut string = "Logged out"

func NewAuthHandler(database db.DB, authCfg *auth.Config) AuthHandler {
	auth := auth.NewAuth(database, authCfg)
	return AuthHandler{database, auth}
}

func (ah AuthHandler) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, err := ReadCookieHandler(r)
		if err != nil {
			code := http.StatusUnauthorized
			http.Error(w, http.StatusText(code), code)
			return
		}

		id, err := ah.Auth.ValidateToken(token)
		if err != nil {
			code := http.StatusUnauthorized
			http.Error(w, http.StatusText(code), code)
			return
		}

		if ah.isTokenInactive(token, id) {
			code := http.StatusUnauthorized
			http.Error(w, http.StatusText(code), code)
			return
		}

		ctx := context.WithValue(r.Context(), authCtx, &contextData{id, token})
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

func (ah AuthHandler) Login(w http.ResponseWriter, r *http.Request) {
	u, err := user.NewUserFromRequest(ah.DB, r.Body)
	if err != nil {
		response.Send(w, "", err)
		return
	}

	id, err := u.Auth()
	if err != nil {
		response.Send(w, "", err)
		return
	}

	token, err := ah.Auth.GenerateToken(id)
	if err != nil {
		response.Send(w, "", err)
		return
	}
	setCookieHandler(w, "Authorization", token)
	response.Send(w, loggedIn, err)
	return
}

func (ah AuthHandler) Logout(w http.ResponseWriter, r *http.Request) {
	cData := r.Context().Value(authCtx).(*contextData)
	err := ah.DB.Redis().Del(cData.token).Err()
	if err != nil {
		log.Println(err)
		response.Send(w, "", response.InternalError)
	}
	response.Send(w, loggedOut, nil)
}

func (ah AuthHandler) isTokenInactive(token, id string) bool {
	rId := ah.DB.Redis().Get(token).Val()
	if rId == "" || id == "" {
		return true
	}

	return rId != id
}

func setCookieHandler(w http.ResponseWriter, name, val string) {
	cookie := &http.Cookie{
		Name:     name,
		Value:    val,
		Domain:   "localhost",
		Path:     "/",
		HttpOnly: true,
	}
	http.SetCookie(w, cookie)
}

func ReadCookieHandler(r *http.Request) (string, error) {
	cookie, err := r.Cookie("Authorization")
	if err != nil {
		log.Println(err)
		return "", err
	}
	return cookie.Value, nil
}
