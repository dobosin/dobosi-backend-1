package handler

import (
	"fmt"
	"log"
	"net/http"
	"text/template"

	"github.com/gorilla/mux"

	"gitlab.com/dobosin/dobosi-backend-1/internal/auth"
	"gitlab.com/dobosin/dobosi-backend-1/internal/db"
)

func Init(database db.DB, authCfg *auth.Config) {
	r := mux.NewRouter()

	uh := NewUserHandler(database)
	r.Use(Middleware)
	r.HandleFunc("/", indexHandler).Methods("GET")
	r.HandleFunc("/user", uh.RegisterUser).Methods("POST")
	r.HandleFunc("/registration", registrationHandler).Methods("GET")

	a := NewAuthHandler(database, authCfg)
	r.HandleFunc("/user/login", a.Login).Methods("POST")

	private := r.PathPrefix("").Subrouter()
	private.Use(a.AuthMiddleware)
	private.HandleFunc("/user/logout", a.Logout).Methods("POST")
	private.HandleFunc("/members", membersHandler).Methods("GET")

	http.Handle("/", r)
}

func Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.URL)

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

		next.ServeHTTP(w, r)
	})
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	t, err := template.ParseFiles("index.html")
	if err != nil {
		fmt.Fprintf(w, "Unable to load template")
	}
	t.Execute(w, "")
}

func membersHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	t, err := template.ParseFiles("members.html")
	if err != nil {
		fmt.Fprintf(w, "Unable to load template")
	}
	t.Execute(w, "")
}

func registrationHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	t, err := template.ParseFiles("registration.html")
	if err != nil {
		fmt.Fprintf(w, "Unable to load template")
	}
	t.Execute(w, "")
}
