package user

import (
	"bytes"
	"encoding/json"
	"io"
	"log"

	"golang.org/x/crypto/bcrypt"

	"gitlab.com/dobosin/dobosi-backend-1/internal/db"
	"gitlab.com/dobosin/dobosi-backend-1/internal/response"
)

type User interface {
	Create() (string, error)
	Auth() (string, error)
}

type UserModul struct {
	DB db.DB
	ud *UserData
}

type UserData struct {
	ID        string   `json:"id"`
	Email     string   `json:"email"`
	Password  string   `json:"password,omitempty"`
	tableName struct{} `pg:"users"`
}

const Success string = "The user has been created"

func NewUserFromRequest(database db.DB, in io.Reader) (User, error) {
	um := UserModul{
		DB: database,
		ud: &UserData{},
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(in)

	err := json.Unmarshal(buf.Bytes(), &um.ud)
	if err != nil {
		log.Println(err)
		return &um, response.InvalidInputError
	}
	return &um, err
}

func (um *UserModul) Create() (string, error) {
	um.ud.ID = ""
	isUserExists, err := um.DB.Pg().Model(&UserData{}).Where("email = ?", um.ud.Email).Exists()
	if err != nil {
		log.Println(err)
		return "", response.InternalError
	}
	if isUserExists {
		return "", ExistsError
	}

	um.ud.Password, err = cryptPassword(um.ud.Password)
	if err != nil {
		log.Println(err)
		return "", response.InternalError
	}

	_, err = um.DB.Pg().Model(um.ud).Insert()
	if err != nil {
		log.Println(err)
		return "", response.InternalError
	}

	um.ud.Password = ""

	return Success, nil
}

func (um *UserModul) Auth() (string, error) {
	loginU := UserData{}
	err := um.DB.Pg().Model(&loginU).Column("id", "password").Where("email = ?", um.ud.Email).Select()
	if err != nil {
		log.Println(err)
		return "", InvalidAuthError
	}

	err = bcrypt.CompareHashAndPassword([]byte(loginU.Password), []byte(um.ud.Password))
	if err != nil {
		if err == bcrypt.ErrMismatchedHashAndPassword {
			return "", InvalidAuthError
		}
		log.Println(err)
		return "", InvalidAuthError
	}
	return loginU.ID, err
}

func cryptPassword(passwd string) (string, error) {
	pass, err := bcrypt.GenerateFromPassword([]byte(passwd), bcrypt.DefaultCost)
	return string(pass), err
}
