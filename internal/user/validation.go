package user

type ErrStr string

func (e ErrStr) Error() string {
	return string(e)
}

const ExistsError ErrStr = "User is already exists"
const InvalidAuthError ErrStr = "Invalid email or password"
