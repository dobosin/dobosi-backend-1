package response

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

type ErrStr string

func (e ErrStr) Error() string {
	return string(e)
}

const InternalError ErrStr = "Something went wrong"
const InvalidInputError ErrStr = "Invalid input data"

func Send(w http.ResponseWriter, data interface{}, err error) {
	var body []byte
	if err == InternalError {
		addHeader(w, http.StatusInternalServerError, string(body))
		return
	}
	if err != nil {
		addHeader(w, http.StatusBadRequest, errorMessage(err))
		return
	}

	body, err = json.Marshal(data)
	if err != nil {
		log.Println(err)
		addHeader(w, http.StatusInternalServerError, string(body))
		return
	}

	addHeader(w, http.StatusOK, successMessage(body))
}

func addHeader(w http.ResponseWriter, statusCode int, data string) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	io.WriteString(w, data)
}

func errorMessage(err error) string {
	return `{"error": "` + err.Error() + `"}`
}

func successMessage(data []byte) string {
	return `{"data": ` + string(data) + `}`
}
