package db

import (
	"fmt"

	"github.com/go-redis/redis"
)

type RConfig struct {
	Host string `envconfig:"REDIS_HOST"`
	Port string `envconfig:"REDIS_PORT"`
}

func newRedisConn(cfg *RConfig) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", cfg.Host, cfg.Port),
		DB:   0,
	})

	err := client.Ping().Err()
	if err != nil {
		panic(err)
	}
	return client
}

func (conn *Connection) Redis() *redis.Client {
	return conn.redis
}

func (conn *Connection) RedisAddr() string {
	return fmt.Sprintf("%s:%s", conn.cfg.Redis.Host, conn.cfg.Redis.Port)
}
