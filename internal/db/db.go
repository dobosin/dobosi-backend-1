package db

import (
	"github.com/go-pg/pg/v10"
	"github.com/go-redis/redis"
)

type DB interface {
	Pg() *pg.DB
	Redis() *redis.Client
	PgAddr() string
	RedisAddr() string
}

type Config struct {
	Pg    *PgConfig
	Redis *RConfig
}

type Connection struct {
	pg    *pg.DB
	redis *redis.Client
	cfg   Config
}

func NewDB(cfg Config) DB {
	var conn Connection
	conn.cfg = cfg

	conn.pg = newPostgresConn(cfg.Pg)
	conn.redis = newRedisConn(cfg.Redis)

	return &conn
}
