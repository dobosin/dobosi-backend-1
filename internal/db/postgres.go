package db

import (
	"context"
	"fmt"

	"github.com/go-pg/pg/v10"
)

type PgConfig struct {
	Host     string `envconfig:"PG_HOST"`
	Port     string `envconfig:"PG_PORT"`
	Database string `envconfig:"PG_DB"`
	User     string `envconfig:"PG_USER"`
	Password string `envconfig:"PG_PWD"`
}

func newPostgresConn(cfg *PgConfig) *pg.DB {
	conn := pg.Connect(&pg.Options{
		Addr:     fmt.Sprintf("%s:%s", cfg.Host, cfg.Port),
		User:     cfg.User,
		Password: cfg.Password,
		Database: cfg.Database,
	})

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	if err := conn.Ping(ctx); err != nil {
		panic(err)
	}

	return conn
}

func (conn *Connection) Pg() *pg.DB {
	return conn.pg
}

func (conn *Connection) PgAddr() string {
	return fmt.Sprintf("%s:%s", conn.cfg.Pg.Host, conn.cfg.Pg.Port)
}
