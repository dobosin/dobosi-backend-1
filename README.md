# dobosi-backend-1

REST API for BE role

## How to run the code in localhost

1. Clone the repo `git clone <url>`
2. Enter to folder `cd dobosi-backend-1`
3. Make a copy from .env.example by using `cp .env.example .env` cmd
4. Edit the .env file
    - Change the PG_HOST value to `postgres`
    - Change the REDIS_HOST value to `redis`
    - Change the JWT_SECRET value to any string for generating jwt token
    - Set TOKEN_EXPIRE value to any integer. The token will be active during this time. (minutes)
5. Set PG_DB, PG_USER, PG_PWD envinrontment variable from `.env` for docker compose. 
    - You can run `export $(grep -v '^#' .env | xargs)` cmd on Linux or Mac
6. Run `docker-compose up -d` cmd to start
7. Open Postgres with any client
    - Enter with the credetion from the `.env` file
    - Run the SQL from the `scripts\db.sql`
    - You might need to rerun `docker-compose up -d` cmd, because the application stops (It does not find the table)
8. Open the `http://localhost:8080/` url from the browser

## Additional info

- I used JWT Token for the authentication. I stored the generated token in a Redis in-memory database. I could have used the refresh method to change the token, however it took more time to develop this project. The JWT Token is stored in cookie in the client side. The server sets it with HttpOnly flag.
- I used Postgres to store the user details and it works with models using ORM.
- I did not wrote any test, and comments for the functions. The production version should contain them.
- The html response handler could be solved nicer. I just added to the `internal/handler/routes.go` file.
- I did not write validation for the user registration. I used the default crypto Go package for hashing the password.

### Hour

- It took around 13 hours for developing this API and the small client. I already created a similar project recently, so it would have been few more hours without that experience.
