module gitlab.com/dobosin/dobosi-backend-1

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-pg/pg/v10 v10.6.2
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/kelseyhightower/envconfig v1.4.0
	golang.org/x/crypto v0.0.0-20201012173705-84dcc777aaee
)
