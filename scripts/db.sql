CREATE TABLE IF NOT EXISTS users (
	id serial UNIQUE,
	email varchar(256) NOT NULL,
	"password" varchar(128) NULL,
	CONSTRAINT users_pkey PRIMARY KEY (id),
	CONSTRAINT users_email_unique UNIQUE (email)
);